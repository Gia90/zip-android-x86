# Zip Info for Android x86

Quick and dirty script to cross compile the "Info-ZIP zip utility" for Android x86 (API >= 23).

# How to
- Install the **android-ndk** (or *better and faster* use a [docker image](https://hub.docker.com/r/bitriseio/android-ndk/))
- Clone the "**Info-Zip**" repo: https://github.com/LuaDist/zip
- Move to the "**unix**" folder in the "Info Zip" project root
- Download the ["build.sh"](build.sh) from this project and edit it (*check the "ANDROID_NDK" full path, "CLANG" and "LD" relative paths and customize it as needed*)
- Run ["build.sh"](build.sh)

> The script is using "**API 23**" as target platform to avoid "**pthread_atfork**" missing reference linking errors ([more info here](https://github.com/openssl/openssl/issues/3901))
