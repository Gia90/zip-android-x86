#!/bin/sh

ANDROID_NDK=/opt/android-ndk/
CLANG=toolchains/llvm/prebuilt/linux-x86_64/bin/clang
LD=toolchains/x86_64-4.9/prebuilt/linux-x86_64/x86_64-linux-android/bin/ld

${ANDROID_NDK}${CLANG} -c -O2 -I. -DUNIX  -w --target=i686-linux-android --gcc-toolchain=${ANDROID_NDK}toolchains/x86_64-4.9/prebuilt/linux-x86_64/ --sysroot=${ANDROID_NDK}sysroot/ -isystem ${ANDROID_NDK}sysroot/usr/include/x86_64-linux-android/ -fPIE -fPIC zip.c
${ANDROID_NDK}${CLANG} -c -O2 -I. -DUNIX  -w --target=i686-linux-android --gcc-toolchain=${ANDROID_NDK}toolchains/x86_64-4.9/prebuilt/linux-x86_64/ --sysroot=${ANDROID_NDK}sysroot/ -isystem ${ANDROID_NDK}sysroot/usr/include/x86_64-linux-android/ -fPIE -fPIC zipfile.c
${ANDROID_NDK}${CLANG} -c -O2 -I. -DUNIX  -w --target=i686-linux-android --gcc-toolchain=${ANDROID_NDK}toolchains/x86_64-4.9/prebuilt/linux-x86_64/ --sysroot=${ANDROID_NDK}sysroot/ -isystem ${ANDROID_NDK}sysroot/usr/include/x86_64-linux-android/ -fPIE -fPIC zipup.c
${ANDROID_NDK}${CLANG} -c -O2 -I. -DUNIX  -w --target=i686-linux-android --gcc-toolchain=${ANDROID_NDK}toolchains/x86_64-4.9/prebuilt/linux-x86_64/ --sysroot=${ANDROID_NDK}sysroot/ -isystem ${ANDROID_NDK}sysroot/usr/include/x86_64-linux-android/ -fPIE -fPIC fileio.c
${ANDROID_NDK}${CLANG} -c -O2 -I. -DUNIX  -w --target=i686-linux-android --gcc-toolchain=${ANDROID_NDK}toolchains/x86_64-4.9/prebuilt/linux-x86_64/ --sysroot=${ANDROID_NDK}sysroot/ -isystem ${ANDROID_NDK}sysroot/usr/include/x86_64-linux-android/ -fPIE -fPIC util.c
${ANDROID_NDK}${CLANG} -c -O2 -I. -DUNIX  -w --target=i686-linux-android --gcc-toolchain=${ANDROID_NDK}toolchains/x86_64-4.9/prebuilt/linux-x86_64/ --sysroot=${ANDROID_NDK}sysroot/ -isystem ${ANDROID_NDK}sysroot/usr/include/x86_64-linux-android/ -fPIE -fPIC globals.c
${ANDROID_NDK}${CLANG} -c -O2 -I. -DUNIX  -w --target=i686-linux-android --gcc-toolchain=${ANDROID_NDK}toolchains/x86_64-4.9/prebuilt/linux-x86_64/ --sysroot=${ANDROID_NDK}sysroot/ -isystem ${ANDROID_NDK}sysroot/usr/include/x86_64-linux-android/ -fPIE -fPIC crypt.c
${ANDROID_NDK}${CLANG} -c -O2 -I. -DUNIX  -w --target=i686-linux-android --gcc-toolchain=${ANDROID_NDK}toolchains/x86_64-4.9/prebuilt/linux-x86_64/ --sysroot=${ANDROID_NDK}sysroot/ -isystem ${ANDROID_NDK}sysroot/usr/include/x86_64-linux-android/ -fPIE -fPIC ttyio.c
${ANDROID_NDK}${CLANG} -c -O2 -I. -DUNIX  -w --target=i686-linux-android --gcc-toolchain=${ANDROID_NDK}toolchains/x86_64-4.9/prebuilt/linux-x86_64/ --sysroot=${ANDROID_NDK}sysroot/ -isystem ${ANDROID_NDK}sysroot/usr/include/x86_64-linux-android/ -fPIE -fPIC unix/unix.c
${ANDROID_NDK}${CLANG} -c -O2 -I. -DUNIX  -w --target=i686-linux-android --gcc-toolchain=${ANDROID_NDK}toolchains/x86_64-4.9/prebuilt/linux-x86_64/ --sysroot=${ANDROID_NDK}sysroot/ -isystem ${ANDROID_NDK}sysroot/usr/include/x86_64-linux-android/ -fPIE -fPIC crc32.c
${ANDROID_NDK}${CLANG} -c -O2 -I. -DUNIX  -w --target=i686-linux-android --gcc-toolchain=${ANDROID_NDK}toolchains/x86_64-4.9/prebuilt/linux-x86_64/ --sysroot=${ANDROID_NDK}sysroot/ -isystem ${ANDROID_NDK}sysroot/usr/include/x86_64-linux-android/ -fPIE -fPIC zbz2err.c
${ANDROID_NDK}${CLANG} -c -O2 -I. -DUNIX  -w --target=i686-linux-android --gcc-toolchain=${ANDROID_NDK}toolchains/x86_64-4.9/prebuilt/linux-x86_64/ --sysroot=${ANDROID_NDK}sysroot/ -isystem ${ANDROID_NDK}sysroot/usr/include/x86_64-linux-android/ -fPIE -fPIC deflate.c
${ANDROID_NDK}${CLANG} -c -O2 -I. -DUNIX  -w --target=i686-linux-android --gcc-toolchain=${ANDROID_NDK}toolchains/x86_64-4.9/prebuilt/linux-x86_64/ --sysroot=${ANDROID_NDK}sysroot/ -isystem ${ANDROID_NDK}sysroot/usr/include/x86_64-linux-android/ -fPIE -fPIC trees.c

${ANDROID_NDK}${LD} \
--sysroot=${ANDROID_NDK}sysroot/ -static -z relro --enable-new-dtags --eh-frame-hdr -m elf_i386 \
-o zip \
${ANDROID_NDK}platforms/android-23/arch-x86/usr/lib/crtbegin_dynamic.o \
-L ${ANDROID_NDK}sysroot/usr/lib/i686-linux-android/ \
-L ${ANDROID_NDK}platforms/android-23/arch-x86/usr/lib/ \
-L ${ANDROID_NDK}toolchains/x86-4.9/prebuilt/linux-x86_64/lib/gcc/i686-linux-android/4.9.x/ \
 zip.o zipfile.o zipup.o fileio.o util.o globals.o crypt.o ttyio.o unix.o crc32.o zbz2err.o deflate.o trees.o \
-lgcc -ldl -lc -lgcc -ldl ${ANDROID_NDK}platforms/android-23/arch-x86/usr/lib/crtend_android.o
